<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ComentariosController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
 
});

Route::get('/createComentario', [ComentariosController::class, 'create']);
/* Rutas Categoria */
Route::get('/categoria/create', [CategoriaController::class, 'create']);
Route::put('/categoria/update', [CategoriaController::class, 'update']);
Route::put('/categoria/delete', [CategoriaController::class, 'delete']);
Route::get('/categoria/edit', [CategoriaController::class, 'edit']);
Route::get('/categoria/ver', [CategoriaController::class, 'ver']);
Route::get('/categoria/verTodo', [CategoriaController::class, 'verTodo']);
/* Rutas Post */
Route::get('/post/create', [PostController::class, 'create']);
Route::put('/post/update', [PostController::class, 'update']);
Route::put('/post/delete', [PostController::class, 'delete']);
Route::get('/post/edit', [PostController::class, 'edit']);
Route::get('/post/ver', [PostController::class, 'ver']);
Route::get('/post/verTodo', [PostController::class, 'verTodo']);
/* Rutas Comentarios */
Route::get('/comentario/create', [ComentariosController::class, 'create']);
Route::put('/comentario/update', [ComentariosController::class, 'update']);
Route::put('/comentario/delete', [ComentariosController::class, 'delete']);
Route::get('/comentario/edit', [ComentariosController::class, 'edit']);
Route::get('/comentario/ver', [ComentariosController::class, 'ver']);
Route::get('/comentario/verTodo', [ComentariosController::class, 'verTodo']);

