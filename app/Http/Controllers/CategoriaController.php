<?php

namespace App\Http\Controllers;
use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $categoria = new Categoria();
            $categoria->nombre = $request->nombre;
            $categoria->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $categoria = Categoria::where('id',$request->id)->first();
            $categoria->nombre = $request->nombre;
            $categoria->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }   
        
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $categoria = Categoria::where('id',$request->id)->first();
            $categoria->delete();
            return response('Eliminado correctamente', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al eliminar información.', 200)->header('Content-Type', 'text/plain');
        }   
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
        $categoria = Categoria::where('id',$request->id)->first();
        $categoria->nombre = $request->nombre;
        $categoria->save();
        return response('Actualizo los datos', 200)->header('Content-Type', 'text/plain');
    } catch (Throwable $e) {
        return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
    }   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver(Request $request)
    {
       
        $categoria = Categoria::where('id',$request->id)->first();
        return $categoria;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        $categorias = Categoria::get();
        return response($categorias, 200)->header('Content-Type', 'text/plain');

    }

  
}
