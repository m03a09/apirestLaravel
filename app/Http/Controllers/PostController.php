<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Categoria;

class PostController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $categoria = Categoria::where('id',$request->Categorias_id)->first();
            $post = new Post();
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->Categorias_id = $categoria->id;
            $post->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $categoria = Categoria::where('id',$request->Categorias_id)->first();
            $post = Post::where('id',$request->id)->first();
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->Categorias_id = $categoria->id;
            $post->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }   
        
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $post = Post::where('id',$request->id)->first();
            $post->delete();
            return response('Eliminado correctamente', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al eliminar información.', 200)->header('Content-Type', 'text/plain');
        }   
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $categoria = Categoria::where('id',$request->Categorias_id)->first();
            $post = Post::where('id',$request->id)->first();
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->Categorias_id = $categoria->id;
            $post->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver(Request $request)
    {
       
        $post = Post::where('id',$request->id)->first();
        return $post;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        $posts = Post::get();
        return response($posts, 200)->header('Content-Type', 'text/plain');

    }

  
}
