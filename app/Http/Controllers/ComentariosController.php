<?php

namespace App\Http\Controllers;
use App\Models\Comentario;
use App\Models\Post;

use Illuminate\Http\Request;

class ComentariosController extends Controller
{ /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $post = Post::where('id',$request->Posts_id)->first();
            $comentario = new Comentario();
            $comentario->contenido = $request->contenido;
            $comentario->Posts_id = $post->id;
            $comentario->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        try {
            $post = Post::where('id',$request->Posts_id)->first();
            $comentario = comentario::where('id',$request->id)->first();
            $comentario->contenido = $request->contenido;
            $comentario->Posts_id = $post->id;
            $comentario->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }   
        
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        try {
            $comentario = comentario::where('id',$request->id)->first();
            $comentario->delete();
            return response('Eliminado correctamente', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al eliminar información.', 200)->header('Content-Type', 'text/plain');
        }   
    }
      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $post = Post::where('id',$request->Posts_id)->first();
            $comentario = comentario::where('id',$request->id)->first();
            $comentario->contenido = $request->contenido;
            $comentario->Posts_id = $post->id;
            $comentario->save();
            return response('Guardó exitosamente los datos', 200)->header('Content-Type', 'text/plain');
        } catch (Throwable $e) {
            return response('Error al guardar información.', 200)->header('Content-Type', 'text/plain');
        }   
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver(Request $request)
    {
       
        $comentario = comentario::where('id',$request->id)->first();
        return $comentario;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        $comentarios = comentario::get();
        return response($comentarios, 200)->header('Content-Type', 'text/plain');
    }

  
}
