<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class comentario extends Model
{
    use HasFactory;
    protected $fillable = [
        'contenido',
        'created_at',
        'updated_at'
    ];
    
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
