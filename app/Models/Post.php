<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    use HasFactory;
    protected $fillable = [
        'titulo',
        'contenido',
        'created_at',
        'updated_at'
    ];
    public function caterogia()
    {
        return $this->belongsTo(Categoria::class);
    }
    public function comentario()
    {
        return $this->hasMany(Comentario::class);
    }
    
}
