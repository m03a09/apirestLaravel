<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->name(),
            'contenido' => $this->faker->text(),
            'Categorias_id'=> rand(1, 10),
            'created_at' => $this->faker->unixTime(),
            'updated_at' =>  $this->faker->unixTime()
        ];
    }
}
