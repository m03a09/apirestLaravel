<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComentarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'contenido' => $this->faker->name(),
            'created_at' => $this->faker->unixTime(),
            'Posts_id'=> rand(1, 10),
            'updated_at' =>  $this->faker->unixTime()
        ];
    }
}
